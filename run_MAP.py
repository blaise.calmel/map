# -*- coding: utf-8 -*-
"""
Created on Fri Feb  4 12:02:22 2022

@author: blais
"""
#========================================
# External imports
#========================================
import sys
import numpy as np
import os
import time
import pandas as pd


if "PYCHARM_HOSTED" in os.environ:
    import matplotlib as mpl
    mpl.use('Qt5Agg')

path_cwd = os.getcwd()
# sys.path.insert(0, path_cwd +'\\qrevint_22_06_22')

#========================================
# Internal imports
#========================================
from open_functions import select_file, open_measurement, new_settings, select_directory
from UI.MplCanvas import MplCanvas
from UI.WTContour import WTContour
from UI.MapTrack import Maptrack
# =============================================================================
# SETTINGS
# =============================================================================
"""SETTINGS
        ----------
        checked_transect_user : 
            Selected transect, if None default QRev checked_transect_idx are selected
        navigation_reference_user :
            Navigation reference : 'BT', 'GPS' or None (select default QRev navigation reference)
        node_horizontal_user: float
            Length of MAP cells, if None automatical value is selected
        node_vertical_user: float
            Depth of MAP cells, if None automatical value is selected
        edge_constant: bool
            Indicates if edge cells should all be the same size
        extrap_option: bool
            Indicates if top/bottom/edges extrapolation should be applied
        interp_option: bool
            Indicates if velocities interpolation should be applied
        track_section: bool
            Indicates if average cross-section should be computed on boat track (True) or 
            on mean velocities direction (False)
        plot: bool
            Indicates if graphics should be plotted and saved
        """
checked_transect_user = None
navigation_reference_user = None
node_horizontal_user = None
node_vertical_user = None

edge_constant = True
extrap_option = True
interp_option = True
track_section = True
plot = True

units = {'L': 1, 'Q': 1, 'A': 1, 'V': 1, 'label_L': '(m)', 'label_Q': '(m3/s)', 'label_A': '(m2)',
                 'label_V': '(m/s)',
                 'ID': 'SI'}

with open(os.getcwd()+'\\path_file.txt', 'r') as f:
    path_window = f.readlines(0)

path_results = path_cwd + '\Results'
if not os.path.exists(path_results):
    os.makedirs(path_results)

# =============================================================================
# Open folder
# =============================================================================
time_start = time.time()
data = {'Parent': [], 'Name': [], 'Meas Q': [], 'MAP Q': [], 'Q diff': []}
path_folder, path_meas, type_meas, name_meas, no_adcp, name_parent = select_directory()
for i in range(len(path_meas)):
    print(f'============== Meas {i} on {len(path_meas)} ==============')
    print(f'TTTTTTTTT Running for : {time.strftime("%H:%M:%S", time.gmtime(time.time() - time_start))} TTTTTTTTT')
    print(f'+++++++++++++ {path_meas[i]} +++++++++++++')
    try:
        meas, checked_transect, navigation_reference = open_measurement(path_meas[i], type_meas[i], apply_settings=True,
                                                                        navigation_reference=navigation_reference_user,
                                                                        checked_transect=checked_transect_user,
                                                                        extrap_velocity=True, run_oursin=True)
    except Exception:
        meas = None

    if meas is not None:
        if all(deg == 0 for deg in meas.transects[meas.checked_transect_idx[0]].sensors.heading_deg.internal.data):
            map_profile = None
        else:
            try:
                map_profile = meas.map
            except Exception:
                map_profile = None

        if map_profile is not None:
            data['Parent'].append(name_parent[i])
            data['Name'].append(name_meas[i])
            data['Meas Q'].append(meas.mean_discharges(meas)['total_mean'])
            data['MAP Q'].append(map_profile.total_discharge)
            if meas.mean_discharges(meas)['total_mean'] != 0:
                per_diff = (map_profile.total_discharge - meas.mean_discharges(meas)['total_mean']) / \
                           meas.mean_discharges(meas)['total_mean']
            else:
                per_diff = np.nan
            data['Q diff'].append(per_diff)
            df = pd.DataFrame(data)
            df.to_csv(path_results+'/data_map.csv', sep=';', encoding='utf-8')

            # Save MAP Water Contour
            vy = map_profile.secondary_velocity
            quiver_label = "Secondary velocity"
            data_quiver = {'x': map_profile.distance_cells_center,
                           'z': map_profile.depth_cells_center,
                           'vy': vy,
                           'vz': map_profile.vertical_velocity,
                           'scale': 0.1,
                           'label': quiver_label}
            bed_profiles = {'x': map_profile.acs_distance, 'depth': map_profile.depth_by_transect}

            map_wt_contour_canvas = MplCanvas(width=15, height=3, dpi=240)
            map_wt_contour_fig = WTContour(canvas=map_wt_contour_canvas)
            map_wt_contour_fig.create(transect=map_profile,
                                       units=units,
                                       data_type='Primary velocity',
                                       data_quiver=data_quiver,
                                       bed_profiles=bed_profiles,
                                       color_map='viridis',
                                       x_axis_type="MAP")
            map_wt_contour_fig.fig.subplots_adjust(left=0.08, bottom=0.2, right=1.05, top=0.97, wspace=0.02, hspace=0)

            map_wt_contour_fig.fig.savefig(path_results + '\\MAP_Profile_' + name_meas[i] + '.png', dpi=300,
                                           bbox_inches='tight')

            # Save MAP ShipTrack
            settings = meas.current_settings()
            map_shiptrack_canvas = MplCanvas(width=4, height=3, dpi=80)
            map_shiptrack_fig = Maptrack(canvas=map_shiptrack_canvas)
            map_shiptrack_fig.create(map_data=map_profile, units=units, nav_ref=settings['NavRef'])
            map_shiptrack_fig.fig.savefig(path_results + '\\MAP_Shiptrack_' + name_meas[i] + '.png', dpi=300,
                                           bbox_inches='tight')

df = pd.read_csv(path_results+'/data_map.csv', sep=';')
df['Q diff'] = df['Q diff']*100
np.median(df['Q diff'])
np.quantile(df['Q diff'], 0.75)
np.quantile(df['Q diff'], 0.25)

import matplotlib.pyplot as plt

markersize_set = 14
elinewidth_set = 7
widths_ge = 0.24
widths_chau = 0.35
linewidth_set = 3
fontsize_choice = 24
plt.rcParams.update({'font.size': fontsize_choice})
plt.rc('legend', **{'fontsize': fontsize_choice - 1})
plt.rc('axes', axisbelow=True)

fig, ax = plt.subplots(1, 1, figsize=(20, 18),gridspec_kw={'hspace': 0.3, 'wspace': 0.0})
boxplot = df.boxplot(column='Q diff', by='Parent', ax=ax, flierprops=dict(marker='.', markerfacecolor=u'#1f77b4',
                     markersize=markersize_set, linewidth=linewidth_set, linestyle='none'),
                     whiskerprops=dict(color=u'#1f77b4'), patch_artist=True, notch=False, return_type="dict")

for box in boxplot[0]['boxes']:
    box.set(color=u'#1f77b4', linewidth=linewidth_set)  # change outline color
    box.set(facecolor='skyblue')  # change fill color
    # box.set_linewidth(linewidth_set)

for box in boxplot[0]['medians']:
    box.set_color(color=u'#1f77b4')
    box.set_linewidth(linewidth_set)
ax.set_ylim(-10, 10)
ax.set_xlabel('Data name', fontsize=fontsize_choice)
ax.set_ylabel('QRevInt - MAP Q difference (%)', fontsize=fontsize_choice)
ax.set_title("")
fig.suptitle('')
fig.savefig(path_results + '\\MAP_test.png', dpi=300, bbox_inches='tight')

df_sort = df.sort_values('Q diff')


# =============================================================================
# Open measurement
# =============================================================================
path_meas, type_meas, name_meas = select_file()


# open ADCP measurement, apply QRevInt filters and user's setting
meas, checked_transect, navigation_reference = open_measurement(path_meas, type_meas, apply_settings=True,
                                                                navigation_reference=navigation_reference_user,
                                                                checked_transect=checked_transect_user,
                                                                extrap_velocity=True, run_oursin=True)


# Optional : Apply new settings (navigation reference / checked transect) without opening meas again
meas, checked_transect, navigation_reference = new_settings(meas, navigation_reference_user, checked_transect_user, extrap_velocity=True)

# =============================================================================
# Run MAP
# =============================================================================
from Classes.MAP import MAP
map_profile = MAP()
map_profile.populate_data(meas,
                          node_horizontal_user,
                          node_vertical_user,
                          extrap_option=True,
                          edges_option=True,
                          interp_option=False,
                          n_burn=None)
