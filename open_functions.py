# -*- coding: utf-8 -*-
"""
Created on Fri Feb 18 14:28:25 2022

@author: blais
"""
# ========================================
# External imports
# ========================================
import os
import glob
import tkinter as tk
import tkinter.filedialog
import numpy as np
import warnings
import sys
import scipy.io as sio

# ========================================
# Internal imports
# ========================================
from Classes.Measurement import Measurement
from Classes.Oursin import Oursin


# =============================================================================
# Functions
# =============================================================================
def select_file(path_window=None):
    if not path_window:
        path_window = os.getcwd()
    # Open a window to select a measurement
    root = tk.Tk()
    root.withdraw()
    root.attributes("-topmost", True)
    path_meas = tk.filedialog.askopenfilenames(parent=root, initialdir=path_window, title='Select ADCP file',
                                               filetypes=[('ADCP data', '.mat .mmt')])
    if path_meas:
        keys = [elem.split('.')[-1].split('.')[0] for elem in path_meas]
        name_folders = [path_meas[0].split('/')[-2].split('.')[0]][0]

        if 'mmt' in keys:
            type_meas = 'TRDI'
            fileName = path_meas[0]
        elif 'mat' in keys:
            type_meas = 'SonTek'
            ind_meas = [i for i, s in enumerate(keys) if "mat" in s]
            fileName = [(path_meas[x]) for x in ind_meas]

        return fileName, type_meas, name_folders
    else:
        warnings.warn('No file selected - end')
        return None, None, None

def select_directory(path_window=None):
    # Open a window to select a folder which contains measurements
    root = tk.Tk()
    root.withdraw()
    root.attributes("-topmost", True)
    path_folder = tk.filedialog.askdirectory(parent=root, initialdir=path_window, title='Select folder')
    if path_folder:
        # ADCP folders path
        path_folder = '\\'.join(path_folder.split('/'))
        path_folders = np.array(glob.glob(path_folder + "/*"))
        # path_folders = [f.path for f in os.scandir(path_folder) if f.is_dir()]
        # Load their name
        name_folders = np.array([os.path.basename(x) for x in path_folders])

        # only for testing
        # path_test = []
        # for id_meas in range(len(path_folders)):
        #     path_test.extend([f.path for f in os.scandir(path_folders[id_meas]) if f.is_dir()])
        # path_folders = path_test
        # name_folders = np.array([os.path.basename(x) for x in path_folders])
        name_parent = [i.split('\\')[-2] for i in path_folders]

        # Exclude files
        excluded_folders = [s.find('.') == -1 for s in name_folders]
        path_folders = path_folders[excluded_folders]
        name_folders = name_folders[excluded_folders]

        # Open measurement
        type_meas = list()
        path_meas = list()
        name_meas = list()
        no_adcp = list()
        for id_meas in range(len(path_folders)):
            list_files = os.listdir(path_folders[id_meas])
            exte_files = list([i.split('.', 1)[-1] for i in list_files])
            if 'mmt' in exte_files or 'mat' in exte_files:
                if 'mat' in exte_files:
                    loca_meas = [i for i, s in enumerate(exte_files) if "mat" in s]  # transects index
                    fileNameRaw = [(list_files[x]) for x in loca_meas]
                    qrev_data = False
                    path_qrev_meas = []
                    for name in fileNameRaw:
                        path = os.path.join(path_folders[id_meas], name)
                        mat_data = sio.loadmat(path, struct_as_record=False, squeeze_me=True)
                        if 'version' in mat_data:
                            path_qrev_meas.append(os.path.join(path_folders[id_meas], name))
                            qrev_data = True
                    if qrev_data:
                        recent_time = None
                        recent_id = 0
                        for i in range(len(path_qrev_meas)):
                            qrev_time = os.path.getmtime(path_qrev_meas[i])
                            if recent_time is None or qrev_time < recent_time:
                                recent_time = qrev_time
                                recent_id = i
                        path = path_qrev_meas[recent_id]
                        mat_data = sio.loadmat(path, struct_as_record=False, squeeze_me=True)
                        print('QRev file')
                        type_meas.append('QRev')
                        name_meas.append(name_folders[id_meas])
                        path_meas.append(mat_data)

                    else:
                        print('SonTek file')
                        type_meas.append('SonTek')
                        fileName = [s for i, s in enumerate(fileNameRaw)
                                    if "QRev.mat" not in s]
                        name_meas.append(name_folders[id_meas])
                        path_meas.append(
                            [os.path.join(path_folders[id_meas], fileName[x]) for x in
                             range(len(fileName))])

                else:
                    print('TRDI file')
                    type_meas.append('TRDI')
                    loca_meas = exte_files.index("mmt")  # index of the measurement file
                    fileName = list_files[loca_meas]
                    path_meas.append(os.path.join(path_folders[id_meas], fileName))
                    name_meas.append(name_folders[id_meas])
            else:
                no_adcp.append(name_folders[id_meas])
                print(f"No ADCP : {name_folders[id_meas]}")
        return path_folder, path_meas, type_meas, name_meas, no_adcp, name_parent
    else:
        warnings.warn('No folder selected - end')
        return None, None, None, None, None, None


def open_measurement(path_meas, type_meas, apply_settings=False, navigation_reference=None,
                     checked_transect=None, extrap_velocity=False, run_oursin=False, use_weighted=False,
                     use_measurement_thresholds=False):
    # Open measurement
    meas = Measurement(in_file=path_meas, source=type_meas, proc_type='QRev', run_oursin=run_oursin,
                       use_weighted=use_weighted, use_measurement_thresholds=use_measurement_thresholds,
                       gps_quality_threshold=1)

    if apply_settings:
        if navigation_reference == 'GPS':
            if not meas.transects[meas.checked_transect_idx[0]].gps:
                print('No GPS available : switch to BT')
                navigation_reference = 'BT'
        meas, checked_transect, navigation_reference = new_settings(meas,
                                                                    navigation_reference,
                                                                    checked_transect,
                                                                    extrap_velocity, run_oursin)
    else:
        if meas.current_settings()['NavRef'] == 'bt_vel':
            navigation_reference = 'BT'
        elif meas.current_settings()['NavRef'] == 'gga_vel':
            navigation_reference = 'GPS'
        checked_transect = meas.checked_transect_idx

    return meas, checked_transect, navigation_reference


def new_settings(meas, navigation_reference_user=None, checked_transect_user=None, extrap_velocity=False,
                 run_oursin=False):
    # Apply settings
    settings = meas.current_settings()

    settings_change = False

    # Default Navigation reference
    if navigation_reference_user is None:
        if meas.current_settings()['NavRef'] == 'bt_vel':
            navigation_reference = 'BT'
        elif meas.current_settings()['NavRef'] == 'gga_vel' or meas.current_settings()['NavRef'] == 'vtg_vel':
            navigation_reference = 'GPS'

    # Change Navigation reference
    else:
        navigation_reference = navigation_reference_user
        if navigation_reference_user == 'BT' and meas.current_settings()['NavRef'] != 'bt_vel':
            settings['NavRef'] = 'BT'
            settings_change = True
        elif navigation_reference_user == 'GPS' and meas.current_settings()['NavRef'] != 'gga_vel':
            settings['NavRef'] = 'GGA'
            settings_change = True

    # Change checked transects
    if not checked_transect_user or checked_transect_user == meas.checked_transect_idx:
        checked_transect_idx = meas.checked_transect_idx
    else:
        checked_transect_idx = checked_transect_user
        meas.checked_transect_idx = []
        for n in range(len(meas.transects)):
            if n in checked_transect_idx:
                meas.transects[n].checked = True
                meas.checked_transect_idx.append(n)
            else:
                meas.transects[n].checked = False
        meas.selected_transects_changed(checked_transect_user)
        # selected_transects_changed already contains apply_settings

    # Apply Extrapolation on velocities
    if extrap_velocity:
        meas.extrap_fit.change_data_type(meas.transects, 'v')
        settings_change = True

    if settings_change:
        meas.apply_settings(settings)
        if run_oursin:
            meas.oursin = Oursin()
            meas.oursin.compute_oursin(meas)

    return meas, checked_transect_idx, navigation_reference
